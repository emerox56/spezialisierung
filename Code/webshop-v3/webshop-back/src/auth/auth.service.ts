import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import {JwtService} from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from 'src/users/entity/user.entity';
import RegisterDto from 'src/users/dto/register.dto';
import { UserDto } from 'src/users/dto/user.dto';

@Injectable()
export class AuthService {
  constructor(private userService: UsersService, private jwtService: JwtService) {}
  
  // public async register(registerDto : RegisterDto){
  //   const hashedPassword = await bcrypt.hash(registerDto.password,10);
  // }

  async vaidateUser(email: string, password: string): Promise<User | null> {
     const user = await this.userService.findByEmailWithPassword(email);
    if(user){
      const passwordsMatch = await AuthService.comparePasswords(password, user.password);
      if(passwordsMatch){
        return user;
      }
    }
    return null;
  }

  public hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password,10);
  }

  public  static comparePasswords(password: string, hash: string): Promise<boolean> {
  return bcrypt.compare(password, hash);
}

  //jwt wird hier assigned 
  async createJwtToken(user:User){
    const payload = {email: user.email, sub: user.id};
    return{
      access_token: this.jwtService.sign(payload),
    }
  }
}
