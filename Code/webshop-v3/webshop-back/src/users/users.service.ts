import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RegisterDto } from './dto/register.dto';
import { UserDto } from './dto/user.dto';
import { User } from './entity/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    public usersRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(id: string): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  findByEmailWithPassword(email: string): Promise<User>{
    return this.usersRepository.createQueryBuilder('user').addSelect('user.password').where({email}).getOne();
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }

  userRegistration() {}

  async create(registerDto: RegisterDto) {
    const { email, password } = registerDto;

    try {
      const user: RegisterDto = this.usersRepository.create({
        email,
        password,
      });
      await this.usersRepository.save(registerDto);

      return user;
    } catch (err) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }
  }


}


