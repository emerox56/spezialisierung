import { IsEmail, IsNotEmpty} from 'class-validator';
import RegisterDto from './register.dto';

export class UserDto extends RegisterDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;
}
