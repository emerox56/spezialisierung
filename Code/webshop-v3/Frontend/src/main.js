import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import store from './store'
import VueMeta from 'vue-meta'
import AOS from 'aos'
import 'aos/dist/aos.css'

Vue.config.productionTip = false

Vue.use(VueMeta, {
  keyName: 'head'
})

new Vue({
  created(){
    AOS.init();
  },
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
