import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ContextIdFactory, ModuleRef } from '@nestjs/core';
import { request } from 'http';
import { User } from 'src/users/entity/user.entity';

//is ok

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService, private moduleRef : ModuleRef) {
    super({
      passReqToCallback: false, usernameField: 'email', passwordField: 'password'
    });
  }

  async validate(email: string, password: string): Promise<User> {
    const user = await this.authService.vaidateUser(email, password);
    if (!user) {
      throw new UnauthorizedException(); //Exception wird automatisch von NestJs übernommen durch solche vorgefertigten Klassen
    }
    return user;
  }
}
