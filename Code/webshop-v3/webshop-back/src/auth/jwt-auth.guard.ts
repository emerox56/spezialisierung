import { ExecutionContext, Injectable, UnauthorizedException} from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { AuthGuard } from "@nestjs/passport";
import { Strategy } from "passport";
import { IS_PUBLIC_KEY } from "src/auth/roles.decorator";

@Injectable()
export class JwtAuthGuard extends AuthGuard('myjwt') {
    constructor(private reflector: Reflector){
        super();
    }

    canActivate(context: ExecutionContext){
       const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
           context.getHandler(),
           context.getClass(),
       ]);
       
       if(isPublic){
           return true;
       }
       return super.canActivate(context);
    }
    handleRequest(err, user, info){
         if( err || !user){
             throw err || new UnauthorizedException("jwt-auth-guard");
         }
        return user;
    }
}