import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Request,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { UsersService } from 'src/users/users.service';
import { RegisterDto } from '../dto/register.dto';
import { AuthService } from '../../auth/auth.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Public } from 'src/auth/roles.decorator';
import { UserDto } from '../dto/user.dto';

@Controller('user')
export class UserController {
  constructor(
    private usersService: UsersService,
    private authService: AuthService,
  ) {}

  @Public()
  @Post('register')
  async create(@Body() { email, password }: RegisterDto) {
    const passwordHash = await this.authService.hashPassword(password);
    return this.usersService.create({ email, password: passwordHash });
  }

  @Public()
  @UseGuards(LocalAuthGuard) //guard guards routes and adds logic between request and routehandler
  @Post('login')
  public async login(@Request() req) {
    const user = req.user;
    if (!user) {
      throw new UnauthorizedException();
    }
    const accessToken = await this.authService.createJwtToken(req.user);
    return {
      accessToken,
    };
  }
}
