import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

axios.interceptors.request.use((config) => {
  const jwt = localStorage.getItem("myjwt");
  if (jwt) {
    config.headers.Authorization = jwt;
  }
  return config;
});

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentUser: null,
  },
  mutations: {
    setLogin(state, user) {
      state.currentUser = user;
    },
  },
  actions: {
    async registerUser(context, user) {
      const response = await axios.post("/api/user/register", user);
      if (response.status == 409) {
        console.log("test");
      }
      context.commit("setCurrentUser", response.data);
    },
    async loginUser(context, user) {
      const response = await axios.post("/api/user/login", user);
      if (response.status == 400) {
        console.log("hellöle");
      }
      console.log("trololo");
      context.commit("setCurrentUser", response.data.user);
      localStorage.setItem("myjwt", response.data.accessToken.access_token);
    },
    logOutUser(context) {
      localStorage.removeItem("myjwt");
      context.commit("setCurrentUser", null);
    },
    async loadCurrentUser(context) {
      const response = await axios.get("/api/user/user");
      context.commit("setCurrentUser", response.data);
    },
  },
  modules: {},
});
