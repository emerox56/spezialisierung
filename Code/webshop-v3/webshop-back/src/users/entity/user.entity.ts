import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Entity()
export class User {
  @PrimaryGeneratedColumn('increment') id: number;

  @Column({
    type: 'varchar',
    nullable: false,
    select: false
  })
  password: string;

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  email: string;

  @BeforeInsert() 
  async hashPassword(){
    console.log("huju")
  }
}
