import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import Register from '../views/Register.vue'
import Products from '../views/Products.vue'
import Team from '../views/Team.vue'
import Login from '../views/Login.vue'
import Basket from '../views/Basket.vue'



Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard
  },
  {
    path: '/team',
    name: 'Team',
    component: Team
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  { 
    path: '/products',
    name: 'Products',
    component: Products
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/basket',
    name: 'Basket',
    component: Basket
  },
]


})
